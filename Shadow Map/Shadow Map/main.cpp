//
//  main.cpp
//  ShadowMapping
//
//  Created by Viet Trinh on 3/1/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define PI 3.14159265359
using namespace std;

/*----- CLASSES DECLARATION ----------*/
//=== class Point ===
class Point{
    float x;
    float y;
    float z;
public:
    Point(){x=0;y=0;z=0;}
    Point(float xp,float yp,float zp){x = xp;y=yp;z=zp;}
    float getXCoord(){return x;}
    float getYCoord(){return y;}
    float getZCoord(){return z;}
};

//=== class Vector ===
class Vector{
public:
    float x;
    float y;
    float z;
    
    Vector(){x = 0.0;y=0.0;z=0.0;}
};

/*----- GLOBAL VARIABLES -------------*/
int button, state = 1;
float gx, gy;
float x_win = 512.0;
float z_win = 512.0;
float obj_material_amb[4] = {0.3,0.5,0.2,1.0};
float obj_material_diff[4] = {0.9,0.9,0.9,1.0};
float obj_material_spec[4] = {0.1,0.1,0.1,1.0};
float VIEWER[3]={-150.0,150.0,-50.0};
float LOOKAT[3]={static_cast<float>(x_win/2.0),0.0,static_cast<float>(z_win/2.0)};
float LIGHT[4] = { 512.0, 250.0, 512.0 , 0.0 };   // x, y, z, w
Vector view_dir;           // direction vector from viewer to look-at-point

double modelview[16],projection[16];
int viewport[4];

double Lmodelview[16],Lprojection[16];
int Lviewport[4];
float Ldepth[512*512];

/*----- FUNCTIONS DECLARATION --------*/
void init();
void screenCameraSetUp();
void screenLightSetUp();
void displayScreen();
void mouseClicks(int but,int sta,int x,int y);
void mouseMoves(int x, int y);
void keyPresses(unsigned char c, int x, int y);

Vector* calculateNormal(Point* a, Point* b, Point* c);
void RotateLeftRight(float angle);
void MoveUpDown(float step);
void MoveLeftRight(float step);

void drawScene();
void getCameraMatrices();
void getLightMatrices();

//===== main function ==================================
int main(int argc, char**argv){
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	
	// Create a window
	glutCreateWindow("Shadow Map");
	glutPositionWindow(0, 0);
	glutReshapeWindow(x_win, z_win);
	
	// Program start here...
	glutDisplayFunc(displayScreen);
	glutMouseFunc(mouseClicks);
	glutMotionFunc(mouseMoves);
    glutKeyboardFunc(keyPresses);
	glutMainLoop();
	return 0;
}

//===== screenSetUp ====================================
void screenCameraSetUp(){
    
    /*------ SET UP 3D SCREEN -------*/
    
    // set up object color
    float obj_light_ambient[4] = { 0.2, 0.2, 0.2, 1.0 };     // r, g, b, a
    float obj_light_diffuse[4] = { 0.8, 0.3, 0.1, 1.0 };     // r, g, b, a
    float obj_light_specular[4] = { 0.8, 0.3, 0.1, 1.0 };    // r, g, b, a
    
    // set up background/terrain color
    float terrain_material_amb_diff[4] = { 0.0, 0.0, 1.0, 1.0 }; // r, g, b, a
    float terrain_material_specular[4] = { 0.0, 1.0, 1.0, 1.0 }; // r, g, b, a
    
    glClearColor(0.0,0.0,0.0,1.0);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0,x_win/z_win,5.0,5000.0);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glViewport(0,0,x_win,z_win);
    
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, obj_light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, obj_light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, obj_light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, LIGHT);
    glEnable(GL_LIGHT0);
	
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, terrain_material_amb_diff);	// set up color for background/terrain
    glMaterialfv(GL_FRONT, GL_SPECULAR, terrain_material_specular);             // set up color for background/terrain
    
    gluLookAt(VIEWER[0],VIEWER[1],VIEWER[2],LOOKAT[0],LOOKAT[1],LOOKAT[2],0,1,0);
    
}

//===== screenSetUp ====================================
void screenLightSetUp(){
    
    /*------ SET UP 3D SCREEN -------*/
    
    // set up object color
    float obj_light_ambient[4] = { 0.2, 0.2, 0.2, 1.0 };     // r, g, b, a
    float obj_light_diffuse[4] = { 0.8, 0.3, 0.1, 1.0 };     // r, g, b, a
    float obj_light_specular[4] = { 0.8, 0.3, 0.1, 1.0 };    // r, g, b, a
    
    // set up background/terrain color
    float terrain_material_amb_diff[4] = { 0.0, 0.0, 1.0, 1.0 }; // r, g, b, a
    float terrain_material_specular[4] = { 0.0, 1.0, 1.0, 1.0 }; // r, g, b, a
    
    glClearColor(0.0,0.0,0.0,1.0);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0,x_win/z_win,5.0,5000.0);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glViewport(0,0,x_win,z_win);
    
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, obj_light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, obj_light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, obj_light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, LIGHT);
    glEnable(GL_LIGHT0);
	
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, terrain_material_amb_diff);	// set up color for background/terrain
    glMaterialfv(GL_FRONT, GL_SPECULAR, terrain_material_specular);             // set up color for background/terrain
    
    gluLookAt(LIGHT[0],LIGHT[1],LIGHT[2],LOOKAT[0],LOOKAT[1],LOOKAT[2],0,1,0);
}

//===== displayScreen ===================================
void displayScreen(){
    
    
    // code here for drawing object...
    cout<< "/*======== PASS 1: LIGHT ===============*/"<<endl;
    screenLightSetUp();
    drawScene();
    getLightMatrices();
    
    glReadPixels(0, 0, 512, 512, GL_DEPTH_COMPONENT, GL_FLOAT, Ldepth);
    
    
    cout<< "/*======== PASS 2: CAMERA ===============*/"<<endl;
    screenCameraSetUp();
    drawScene();
    getCameraMatrices();
    
    float depth[512*512],color[512*512*3];
    GLdouble objx,objy,objz;
    GLdouble winx,winy,winz;
    GLint ret;
    
    glReadPixels(0, 0, 512, 512, GL_DEPTH_COMPONENT, GL_FLOAT, depth);
    glReadPixels(0, 0, 512, 512, GL_RGB,GL_FLOAT, color);
    
    for (int i=0; i<512; i++) {
        for (int j=0; j<512; j++) {
            gluUnProject(i, j, depth[i+j*512], modelview, projection, viewport, &objx, &objy, &objz);
            ret = gluProject(objx, objy, objz, Lmodelview, Lprojection, Lviewport, &winx, &winy, &winz);
            
            if(ret==GLU_FALSE){ cout << "project to Light view FAILED"<<endl;}
            
            int px = winx;
            int py = winy;
            
            if(px>=0&&px<512&&py>=0&&py<512){
                float d =Ldepth[px+py*512];
                float wz = winz- (winz*100 - ((int)(winz*100)))/100;
                if((wz-d) > 0.001){
                    color[(i+j*512)*3+0] = 0.8*0.2+0.2*color[(i+j*512)*3+0];
                    color[(i+j*512)*3+1] = 0.8*0.2+0.2*color[(i+j*512)*3+1];
                    color[(i+j*512)*3+2] = 0.8*0.2+0.2*color[(i+j*512)*3+2];
                }
            }
        }
    }
    
    glDrawPixels(512, 512, GL_RGB, GL_FLOAT, color);
    glutSwapBuffers();
}

//===== mouseClicks ====================================
void mouseClicks(int but,int sta,int x,int y){
    button = but;
    state = sta;
    
    
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
		
        gx = float(x)/x_win;
        gy = float(z_win-y)/z_win;
        
        // code here...
    }
    
    glutPostRedisplay();
    
}

//===== mouseMoves ====================================
void mouseMoves(int x, int y){
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
		gx = float(x)/x_win;
        gy = float(z_win-y)/z_win;
        
        // code here..
        
    }
    
    glutPostRedisplay();
    
}

//===== keyPresses ====================================
void keyPresses(unsigned char c, int x, int y){
    
    if (c =='w'){ MoveUpDown(10.0);}
    else if (c =='x'){ MoveUpDown(-10.0);}
    else if (c =='q'){ MoveLeftRight(10.0);}
    else if (c =='e'){ MoveLeftRight(-10.0);}
    else if (c =='a'){ RotateLeftRight(10*PI/180.0);}
    else if (c =='d'){ RotateLeftRight(-10*PI/180.0);}
    
    //more code here...
    
    glutPostRedisplay();
}

//=================================================================
Vector* calculateNormal(Point* a, Point* b, Point* c){
    /* Right hand rule: Thumb is the direction of normal vector,
     curve of fingers is the orientation of vertices
     n = ab x bc
     */
    
    Vector* ret = new Vector();
    Vector v1,v2,v;
    float d;
    
    // Vector ab
    v1.x = b->getXCoord() - a->getXCoord();
    v1.y = b->getYCoord() - a->getYCoord();
    v1.z = b->getZCoord() - a->getZCoord();
    
    // Vector bc
    v2.x = c->getXCoord() - b->getXCoord();
    v2.y = c->getYCoord() - b->getYCoord();
    v2.z = c->getZCoord() - b->getZCoord();
    
    // Vector ab x bc
    v.x = v1.y*v2.z - v2.y*v1.z;
    v.y = v1.z*v2.x - v2.z*v1.x;
    v.z = v1.x*v2.y - v2.x*v1.y;
    
    d = sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
    
    // Normalize vector ab x bc
    ret->x = v.x/d;
    ret->y = v.y/d;
    ret->z = v.z/d;
    
    return ret;
}

//=================================================================
void MoveUpDown(float step){
    
    float distance;
    
    // normalize direction vector
    view_dir.x = LOOKAT[0] - VIEWER[0];
    view_dir.y = LOOKAT[1] - VIEWER[1];
    view_dir.z = LOOKAT[2] - VIEWER[2];
    
    distance = sqrt(view_dir.x*view_dir.x + view_dir.y*view_dir.y + view_dir.z*view_dir.z);
    
    view_dir.x = view_dir.x/distance;
    view_dir.y = view_dir.y/distance;
    view_dir.z = view_dir.z/distance;
    
    // translate viewer and look-at-point positions
    VIEWER[0] += step*view_dir.x;
    VIEWER[2] += step*view_dir.z;
    LOOKAT[0] += step*view_dir.x;
    LOOKAT[2] += step*view_dir.z;
    
}

//=================================================================
void MoveLeftRight(float step){
    
    //    float distance;
    //
    //    // normalize direction vector
    //    view_dir.x = LOOKAT[0] - VIEWER[0];
    //    view_dir.y = LOOKAT[1] - VIEWER[1];
    //    view_dir.z = LOOKAT[2] - VIEWER[2];
    //
    //    distance = sqrt(view_dir.x*view_dir.x + view_dir.y*view_dir.y + view_dir.z*view_dir.z);
    //
    //    view_dir.x = view_dir.x/distance;
    //    view_dir.y = view_dir.y/distance;
    //    view_dir.z = view_dir.z/distance;
    //
    //    // translate viewer and look-at-point positions
    //    VIEWER[0] += step*view_dir.x;
    //    VIEWER[2] += step*view_dir.z;
    //    LOOKAT[0] += step*view_dir.x;
    //    LOOKAT[2] += step*view_dir.z;
    
}

//=================================================================
void RotateLeftRight(float angle){
    float x1,y1,z1,x2,y2,z2;
    
    // translate to origin
    x1 = LOOKAT[0] - VIEWER[0];
    y1 = LOOKAT[1] - VIEWER[1];
    z1 = LOOKAT[2] - VIEWER[2];
    
    // rotate around a pivot
    x2 = x1*cos(angle) + z1*sin(angle);
    y2 = y1;
    z2 = -x1*sin(angle) + z1*cos(angle);
    
    // translate back
    LOOKAT[0] = x2 + VIEWER[0];
    LOOKAT[1] = y2 + VIEWER[1];
    LOOKAT[2] = z2 + VIEWER[2];
}

//=================================================================
void drawScene(){
    
    // draw the terrain
    // a x_win x z_win rectangle with lower left corner (0,0,0)
    // up direction is y
    glBegin(GL_QUADS);
    glNormal3f(0.0,1.0,0.0);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(x_win,0.0,0.0);
    glVertex3f(x_win,0.0,z_win);
    glVertex3f(0.0,0.0,z_win);
    glEnd();
    
    obj_material_amb[0] = 165.0/255.0 ;
    obj_material_amb[1] = 42.0/255.0 ;
    obj_material_amb[2] = 42.0/255.0;
    obj_material_amb[3] = 1.0;
    
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, obj_material_diff);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, obj_material_spec);
    
    glPushMatrix();
    glTranslatef(x_win/2, 100, z_win/2);
    glScaled(1.0, 1.0, 1.0);
    glutSolidCube(50.0);
    glPopMatrix();
    
    obj_material_amb[0] = 89.0/255.0 ;
    obj_material_amb[1] = 123.0/255.0 ;
    obj_material_amb[2] = 42.0/255.0;
    obj_material_amb[3] = 1.0;
    
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, obj_material_diff);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, obj_material_spec);
    
    glPushMatrix();
    glTranslatef(x_win/2-50, 100, z_win/2+50);
    glScaled(2.0, 2.0, 2.0);
    glutSolidSphere(15, 20, 20);
    glPopMatrix();
    
}

//=================================================================
void getCameraMatrices(){
    
    
    glGetDoublev(GL_PROJECTION_MATRIX, projection);
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    glGetIntegerv(GL_VIEWPORT,  viewport);
    
}

//=================================================================
void getLightMatrices(){
    
    
    glGetDoublev(GL_PROJECTION_MATRIX, Lprojection);
    glGetDoublev(GL_MODELVIEW_MATRIX, Lmodelview);
    glGetIntegerv(GL_VIEWPORT,  Lviewport);
    
}
